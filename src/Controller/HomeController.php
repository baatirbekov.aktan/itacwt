<?php

namespace App\Controller;

use App\Entity\Country;
use App\Form\TaxType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(public EntityManagerInterface $entityManager)
    {
    }

    #[Route('/home', name: 'app_home')]
    public function index(): Response
    {
        $form = $this->createForm(TaxType::class);

        return $this->render('home/index.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/check', name: 'check', methods: ['POST'])]
    public function check(Request $request): Response
    {
        $form = $this->createForm(TaxType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productPrice = $form->getData()['product']->getPrice();

            $country = $this->entityManager
                ->getRepository(Country::class)
                ->findOneBy(['code' => strtoupper(substr($request->get('tax')['code'], 0, 2))]);

            $finalPrice = $productPrice + ($productPrice / 100 * $country->getPercent());
            return $this->json([
                'finalPrice' => round($finalPrice, 2),
                'form' => $this->render('home/_form.html.twig', ['form' => $form]
                )], 200);
        }

        return $this->render('home/_form.html.twig', [
            'form' => $form,
        ]);
    }

}
