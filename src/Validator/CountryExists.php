<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class CountryExists extends Constraint
{
    /**
     * @var string
     */
    public string $message = 'The country with code "{{ value }}" does not exist or not valid.';

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return CountryExistsValidator::class;
    }
}
