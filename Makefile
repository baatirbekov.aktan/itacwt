stop:
	docker compose stop
shell:
	docker compose exec app sh
start:
	docker compose up --detach
pull:
	docker compose pull
destroy:
	docker compose down --remove-orphans --volumes
build:
	docker compose build
seed:
	docker compose exec app php bin/console d:f:l --append
migrate:
	docker compose exec app php bin/console d:s:u --force
npm-install:
	docker compose exec app npm install
npm-build:
	docker compose exec app npm run dev
composer-install:
	docker compose exec app composer install
copy-env:
	cp .env.test .env
init: copy-env destroy build start composer-install migrate seed npm-install npm-build

