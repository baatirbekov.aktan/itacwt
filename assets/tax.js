$(() => {
    $(document).on('submit', $('#tax_submit'), function (e) {
        e.preventDefault();
        const form = $('#tax_form');
        const data = $(form).serialize();
        $.ajax({
            method: 'POST',
            url: `/check`,
            cache: false,
            data: data,
            success: function (successResponse) {
                $(form).replaceWith(successResponse.form.content)
                $('#total').text(successResponse.finalPrice)
            },
            error: function (errorResponse) {
                $(form).replaceWith(errorResponse.responseText)
            },
        })
    })
})